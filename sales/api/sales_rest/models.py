from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
        import_href = models.CharField(max_length=200, unique=True)
        vin = models.CharField(max_length=17, unique=True)
        sold = models.BooleanField(default=False)

        def get_api_url(self):
            return reverse("api_delete_salesperson", kwargs={"pk": self.pk})

        def __str__(self):
            return self.vin




class Customer(models.Model):
        first_name = models.CharField(max_length=150)
        last_name = models.CharField(max_length=150)
        address = models.CharField(max_length=150)
        phone_number = models.CharField(max_length=150)

        def get_api_url(self):
            return reverse("api_delete_customer", kwargs={"pk": self.pk})




class Salesperson(models.Model):
        first_name = models.CharField(max_length=150)
        last_name = models.CharField(max_length=150)
        employee_id = models.CharField(max_length=150)

        def get_api_url(self):
            return reverse("api_delete_salesperson", kwargs={"pk": self.pk})




class Sale(models.Model):
        price = models.PositiveIntegerField()

        automobile = models.ForeignKey(
            AutomobileVO,
            related_name="automobile",
            on_delete=models.PROTECT
        )
        salesperson = models.ForeignKey(
            Salesperson,
            related_name="salesperson",
            on_delete=models.PROTECT
        )

        customer = models.ForeignKey(
            Customer,
            related_name="customer",
            on_delete=models.PROTECT
        )

        def get_api_url(self):
            return reverse("api_delete_salesperson", kwargs={"pk": self.pk})
