import React, { useEffect, useState } from "react";

function VehicleList() {
    const [models, setModels] = useState([]);

    const getModelData = async () => {
        const modelResponse = await fetch('http://localhost:8100/api/models/');

        if (modelResponse.ok) {
            const modelData = await modelResponse.json();
            setModels(modelData.models)
        }
    };

    useEffect(() => {
        getModelData()
    }, [])

    return (
        <table className="table align-middle">
        <thead>
            <tr>
                <th>Name</th>
                <th>Picture</th>
                <th>Manufacturer</th>
            </tr>
        </thead>
        <tbody>
            {models.map(model => {
                return (
                    <tr key={ model.id }>
                        <td>{ model.manufacturer.name }</td>
                        <td>{ model.name }</td>
                        <td><img src={model.picture_url} alt={model.name}width="300px" height="200px"/></td>
                    </tr>
                );
            })}
        </tbody>
    </table>
    );
}

export default VehicleList
