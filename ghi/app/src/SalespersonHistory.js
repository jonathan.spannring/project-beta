import React, { useEffect, useState } from 'react';


function FilterList() {
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [sales, setSales] = useState([]);

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);

    }

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        const salesResponse = await fetch('http://localhost:8090/api/sales/');

        if (salesResponse.ok) {
            const salesData = await salesResponse.json();
            setSales(salesData.sales);
        }

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    const filteredSales = sales.filter(sale => {
        return !salesperson || sale.salesperson_id === salesperson;
    });


    return (
        <div className="container px-4 py-4 text-center">
            <h1>Salesperson History</h1>
            <div className="mb-3">
                <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                    <option value="">Select a Salesperson</option>
                    {salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.id} value={salesperson.employee_id}>
                                {salesperson.first_name} {salesperson.last_name}
                            </option>
                        );
                    })}
                </select>
            </div>
            <table className="table table-sm align-middle">
                <thead>
                    <tr>
                        <th>Salesperson ID</th>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredSales.map(sale => {
                        return (
                            <tr key={sale.href}>
                                <td>{sale.salesperson_id}</td>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile}</td>
                                <td>{sale.price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default FilterList;
