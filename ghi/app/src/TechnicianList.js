import React, { useEffect, useState } from "react";

function TechniciansList() {
    const [technicians, setTechnicians] = useState([]);

    const getTechnicianData = async () => {
        const technicianResponse = await fetch('http://localhost:8080/api/technicians/');

        if (technicianResponse.ok) {
            const technicianData = await technicianResponse.json();
            setTechnicians(technicianData.technicians)
        }
    };

    useEffect(() => {
        getTechnicianData()
    }, [])

    return (
    <table className="table table-hover align-middle">
        <thead>
            <tr>
                <th>First name</th>
                <th>Last name</th>
                <th>Employee ID</th>
            </tr>
        </thead>
        <tbody>
            {technicians.map(technician => {
                return (
                    <tr key={technician.id}>
                        <td>{ technician.first_name }</td>
                        <td>{ technician.last_name }</td>
                        <td>{ technician.employee_id }</td>
                    </tr>
                );
            })}
        </tbody>
    </table>
        );
}

export default TechniciansList;
