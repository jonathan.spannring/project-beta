function MainPage() {
  return (
    <div className="px-4 py-3 my-5 text-center">
      <h1 className="display-5 fw-bold ">車車</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4 fw-bold">
          この所は一番最高の自動車の管理職サイトです。
        </p>
      </div>
      <div id="carousel" className="carousel slide">
        <div className="carousel-indicators">
          <button type="button" data-bs-target="carousel" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="carousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="carousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img src="https://images.pexels.com/photos/12608881/pexels-photo-12608881.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" className="d-block w-100" alt="Two Skylines"/>
          </div>
          <div className="carousel-item">
            <img src="https://images.pexels.com/photos/7749314/pexels-photo-7749314.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" className="d-block w-100" alt="Road"/>
          </div>
          <div className="carousel-item">
            <img src="https://images.pexels.com/photos/8960863/pexels-photo-8960863.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" className="d-block w-100" alt="Car Example"/>
          </div>
        </div>
        <button className="carousel-control-prev" type="button" data-bs-target="#carousel" data-bs-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button className="carousel-control-next" type="button" data-bs-target="#carousel" data-bs-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>
      <div className="d-grid gap-2 py-4">
        <a className="btn btn-secondary" href="/sales/new" role="button">Add Sale</a>
        <a className="btn btn-secondary" href="/appointments/new" role="button">Add Service Appointment</a>
        <a className="btn btn-secondary" href="/automobiles" role="button">Inventory List</a>
      </div>
    </div>
  );
}

export default MainPage;
