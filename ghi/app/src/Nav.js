import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item dropdown">
                  <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Salespeople</NavLink>
                  <ul className="dropdown-menu bg-dark">
                  <li className="nav-item"><NavLink className="nav-link" to="/salespeople/new">Add Salesperson</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/salespeople">Salespeople List</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/sales/history">Salesperson History</NavLink></li>
                  </ul>
                </li>
                <li className="nav-item dropdown">
                  <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Customers</NavLink>
                  <ul className="dropdown-menu bg-dark">
                  <li className="nav-item"><NavLink className="nav-link" to="/customers/new">Add Customer</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/customers">Customer List</NavLink></li>
                  </ul>
                </li>
                <li className="nav-item dropdown">
                  <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Sales</NavLink>
                  <ul className="dropdown-menu bg-dark">
                  <li className="nav-item"><NavLink className="nav-link" to="/sales/new">Create Sale</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/sales">Sales List</NavLink></li>
                  </ul>
                </li>
                <li className="nav-item dropdown">
                  <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Technicians</NavLink>
                  <ul className="dropdown-menu bg-dark">
                  <li className="nav-item"><NavLink className="nav-link" to="/technicians/new">Add Technician</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/technicians">Technician List</NavLink></li>
                  </ul>
                </li>
                <li className="nav-item dropdown">
                  <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Service</NavLink>
                  <ul className="dropdown-menu bg-dark">
                  <li className="nav-item"><NavLink className="nav-link" to="/appointments/new">Create Appointment</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/appointments">Appointments List</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/appointments/history">Service History</NavLink></li>
                  </ul>
                </li>
                <li className="nav-item dropdown">
                  <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Automobiles</NavLink>
                  <ul className="dropdown-menu bg-dark">
                  <li className="nav-item"><NavLink className="nav-link" to="/automobiles/new">Add Automobile</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/automobiles">Automobile List</NavLink></li>
                  </ul>
                </li>
                <li className="nav-item dropdown">
                  <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Manufacturers</NavLink>
                  <ul className="dropdown-menu bg-dark">
                  <li className="nav-item"><NavLink className="nav-link" to="/manufacturers/new">Add Manufacturer</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/manufacturers">Manufacturer List</NavLink></li>
                  </ul>
                </li>
                <li className="nav-item dropdown">
                  <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Models</NavLink>
                  <ul className="dropdown-menu bg-dark">
                  <li className="nav-item"><NavLink className="nav-link" to="/models/new">Add Vehicle Model</NavLink></li>
                  <li className="nav-item"><NavLink className="nav-link" to="/models">Model List</NavLink></li>
                  </ul>
                </li>
              </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
