import React, { useState, useEffect } from "react";

function AppointmentList({ }) {
  const [appointments, setAppointments] = useState([]);

  function getAppointments() {
    fetch("http://localhost:8080/api/appointments/")
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        }
      })
      .then((response) => {
        const data = response.appointments.map((appointment) => {
          const dateTime = new Date(appointment.date_time);
          const date = dateTime.toLocaleDateString("en-US");
          const time = dateTime.toLocaleTimeString("en-US").replace(":00 ", " ");
          appointment.date = date;
          appointment.time = time;
          return appointment;
        });
        setAppointments(data);
      });
  }

  useEffect(() => {
    getAppointments();
  }, []);

  function handleCancel(event) {
    const id = event.target.getAttribute("status");
    const url = "http://localhost:8080/api/appointments/" + id + "/cancel/";
    fetch(url, {
      method: "PUT",
    })
      .then((response) => {
        if (response.status === 200) {
          window.location.href = "/appointments";
        }
      })
  }

  function handleFinish(event) {
    const id = event.target.getAttribute("status");
    const url = "http://localhost:8080/api/appointments/" + id + "/finish/";
    fetch(url, {
      method: "PUT",
    })
      .then((response) => {
        if (response.status === 200) {
          window.location.href = "/appointments";
        }
      })
  }

  return (
    <div>
      <h1>Service Appointments</h1>
      <table className="table table-hover align-middle">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
            {appointments.filter(current => current.status === "Created")
              .map((appointment) => (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.purchased_here ? 'Yes' : 'No'}</td>
                <td>{appointment.customer}</td>
                <td>{appointment.date}</td>
                <td>{appointment.time}</td>
                <td>{appointment.technician["first_name"] +
                  " " + appointment.technician["last_name"]}</td>
                <td>{appointment.reason}</td>
                <td>
                  <button className="btn btn-danger btn-small"
                    status={appointment.id} onClick={handleCancel}>
                    Cancel
                  </button>
                  <button className="btn btn-success btn-small mx-2"
                    status={appointment.id} onClick={handleFinish}>
                    Finish
                  </button>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentList;
