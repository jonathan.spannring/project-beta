import React, {useState} from 'react';

function TechnicianForm ({}) {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeID] = useState('');
	  const [submitted, setSubmitted] = useState(false);

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleEmployeeIDChange = (event) => {
        const value = event.target.value;
        setEmployeeID(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;

        const technicianURL = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const technicianResponse = await fetch(technicianURL, fetchConfig);
        if (technicianResponse.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeID('');
            setSubmitted(true);
        }
    }

  return (
          <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Technician</h1>
              <form onSubmit={e => handleSubmit(e)} id="create-technician-form">
                <div className="form-floating mb-3">
                  <input onChange={handleFirstNameChange} placeholder="First name"
                  required type="text" value={first_name} name="first_name" id="first_name"
                  className="form-control"/>
                  <label htmlFor="first_name">First name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleLastNameChange} placeholder="Last name"
                  required type="text" value={last_name} name="last_name" id="last_name"
                  className="form-control"/>
                  <label htmlFor="last_name">Last name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleEmployeeIDChange} placeholder="EmployeeID"
                  required type="text" value={employee_id} name="employee_id" id="employee_id"
                  className="form-control"/>
                  <label htmlFor="employee_id">Employee ID</label>
                </div>
                <button className="btn btn-success">Add +</button>
              </form>
              {submitted && (
                <div
                  className="alert alert-dark mb-0 p-4 mt-4"
                  id="success-message">
                  Your technician has been created!
                </div>
					)}
            </div>
          </div>
        </div>
      );
  }

export default TechnicianForm;
