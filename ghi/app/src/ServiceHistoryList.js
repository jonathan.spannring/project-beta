import React, { useState, useEffect } from "react";

function ServiceHistoryList({}) {
  const [appointments, setAppointments] = useState([]);
  const [vinSearch, setVinSearch] = useState([]);

  function handleSubmit(event) {
    event.preventDefault();
    const searchFor =
      document.querySelector('input[name="searchSelector"]:checked').value;
    const searchWith =
      document.getElementById("inputVinSearch").value.toUpperCase();
    if (searchWith === "") return setVinSearch(appointments);
    const filteredAppointmentData = appointments.filter((appointment) => {
      if (searchFor === "vin") {
        return appointment.vin.toUpperCase().includes(searchWith.toUpperCase());
      }
      return false;
    });
    setVinSearch(filteredAppointmentData);
  }

  function getAppointments() {
    fetch("http://localhost:8080/api/appointments/")
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        }
      })
      .then((response) => {
        const dataAppointment = response.appointments.map((appointment) => {
          const dateTime = new Date(appointment.date_time);
          const date = dateTime.toLocaleDateString("en-US");
          const time = dateTime.toLocaleTimeString("en-US").replace(":00 ", " ");
          appointment.date = date;
          appointment.time = time;
          return appointment;
        });
        setAppointments(dataAppointment);
        setVinSearch(dataAppointment);
      });
  }

  useEffect(() => {
    getAppointments();
  }, []);

  return (
    <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Service Appointments</h1>
            <form onSubmit={handleSubmit} className="d-flex">
                <div className="form-check mb-3">
                  <input className="form-check-input" type="radio" name="searchSelector"
                    id="searchSelector" value={"vin"} defaultChecked />
                  <label className="form-check-label" htmlFor="flexRadioDefault1">
                    VIN
                  </label>
                </div>
                <div className="form-floating mb-3">
                  <input type="text" className="form-control" id="inputVinSearch"
                    placeholder="17 digit VIN here"/>
                <button type="submit" className="btn btn-secondary">Search</button>
              </div>
            </form>
          </div>
          <table className="table table-striped shadow">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {vinSearch.map((appointment) => (
                <tr key={appointment.id}>
                  <td>{appointment.vin}</td>
                  <td>{appointment.sold_here ? 'Yes' : 'No'}</td>
                  <td>{appointment.customer}</td>
                  <td>{appointment.date}</td>
                  <td>{appointment.time}</td>
                  <td>{appointment.technician["first_name"] +
                    " " + appointment.technician["last_name"]}</td>
                  <td>{appointment.reason}</td>
                  <td>{appointment.status}</td>
                </tr>
              ))}
            </tbody>
          </table>
    </div>
  );
}

export default ServiceHistoryList;
