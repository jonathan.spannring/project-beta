import React, { useEffect, useState } from "react";

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);

    const getManufacturerData = async () => {
        const manufacturerResonse = await fetch('http://localhost:8100/api/manufacturers/');

        if (manufacturerResonse.ok) {
            const getManufacturerData = await manufacturerResonse.json();
            setManufacturers(getManufacturerData.manufacturers)
        }
    };

    useEffect(() => {
        getManufacturerData()
    }, [])

    return (
        <table className="table table-hover align-middle">
        <thead>
            <tr>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
            {manufacturers.map(manufacturer => {
                return (
                    <tr key={manufacturer.id}>
                        <td>{ manufacturer.name }</td>
                    </tr>
                );
            })}
        </tbody>
    </table>
    );
}

export default ManufacturerList
