# CarCar
CarCar is a car dealership management application.
It allows a dealership to manage and view and maintain
inventory, manage sales, and manage automobile service
apointments.


Team:

* Max Ruhr - Sales
* Jonathan Spannring - Services

## Design

The CarCar application contains 3 microservices:
- Inventory
- Sales
- Services

## Diagram
![Domain Diagram](<Screen Shot 2024-02-07 at 4.39.21 PM.png>)

## How to Run this App
Install Docker, Git, and Node.js 18.2 or later

Step 1: Fork this Repository and clone it to your machine
Step 2: Build Docker Containers and create the volume for the application

In the proper project folder, run these Terminal Commands:
docker volume create beta-data
docker-compose build
docker-compose up

Step 3: Once the containers are built and running, view the project in browser at http://localhost:3000/

## Service microservice
The Technician model allows a dealership to create and manage their technicians on staff. Technicians are listed by their first name, last name, and their employee identification
Explain your models and integration with the inventory
microservice, here.

The Appointment model allows for the creation of a service appointment. The Appointment Form allows for input of the vehicle's VIN, customer name, select a date and time, choose a technician from avaliable technicians on hand, and give a short summary as to the reason for the appointment. If the vehicle's VIN is from the dealership's inventory then that is reflected when viewing the appointments as if the customer is a VIP or not. Viewing the list of appointments also allows the user to mark appointments as canceled or finished.

Using the data from the Appointment model you can view all Service History and search by a vehicle's VIN to see each vehicle's history and get a summary of each service appointment as well as the current status of the appointment.

## Service Value Objects
The Service microservice ultilizes a single Automobile value object. Using the AutomobileVO the Service microservice has access to automobile models in the Inventory Microservice. AutomobileVO starts with false as the value in the sold field as to allow it to be changed in the future on the Sales microservice side of operations and has a field for VIN's.

## Service API Documentation
The Service API consists of Technicians, Appointments, and Service Appointment History, while also accessing information from the Inventory API.

### Technicians
GET Request to http://localhost:8080/api/technicians/

    - Displays a list of all technicians by First name, Last name, Employee ID

POST Request to http://localhost:8080/api/technicians/

    - Allows for the creation of a new technician

JSON Body -
{
	"first_name": "Mike",
	"last_name": "Walters",
	"employee_id": "mwalters"
}

DELETE Request to http://localhost:8080/api/technicians/:id/

    - Allows the deletion of a technician by id number

### Appointments
GET Request to http://localhost:8080/api/appointments/

    - Displays a list of all appointments with a status of Created. Appointments with a status of Canceled or Completed will be shown in the service history

POST Request to http://localhost:8080/api/appointments/

    - Allows for the creation of a new appointment

JSON Body -
{
	"date_time": "2024-02-07T15:26:00+00:00",
	"reason": "Tires",
	"status": false,
	"vin": "ASD12376AJFYTHFDS",
	"customer": "Buster"
}

DELETE Request to http://localhost:8080/api/appointments/:id/

    - Allows for the deletion of an appointment by ID number, in doing so will make this appointment not show up in the service history

PUT Request to http://localhost:8080/api/appointments/:id/cancel/
PUT Request to http://localhost:8080/api/appointments/:id/finish/

    - Allows the user to change the status to canceled or completed depending on the situation, in doing so will make this appointment show up in the service history


## Sales microservice
The Sales Microservice allows a dealership to manage their Salespeople by creating, deleting, and listing
each Salesperson, as well as viewing tables of filtered information for each Salesperson.

Customer lists are also able to be created and used to store information such as name, contact info, and addresses.

Sales can be listed and created by completing sales forms thorugh the "Add Sale" tab. The Sale form takes unsold automobiles in
inventory, customers within the database, and a salesperson, as well as price. Upon creation of a sale, the user is redirected to a table
showing the created sale along with all relevant information. The Automobile is also removed from sellable inventory.


## Sales Value Objects
The Sales microservice contains one value object - AutomobileVO
AutomobileVO is used to access instances of the Automobile model in the Inventory Microservice.
The AutomobileVO will start with a default value of "false" in the "sold" field to allow it to be
accessed by its VIN number in the create sale form, and upon creation of sale, the "sold" field will change to "true" and
remove it from available automobiles.

## Sales API Documentation
The Sales API consists of Sales, Customers, and Salespeople, as well as accessing Automobiles in Inventory:

### Sales:

GET Request to http://localhost:8090/api/sales/

While display all sales information- Salesperson, Customer, Automobile, Price, and ID.

Create a Sale:

Post Request to http://localhost:8090/api/sales/

JSON Body.. (passing in ID's of each field, as along with integer values in price only)
{
	"automobile": 1,
	"salesperson": 2,
	"customer": 2,
	"price": "5000"
}


### Customers

GET Request to http://localhost:8090/api/customers/

Will display each customers name, address, phone number, and ID (which can be used to Delete the customer via Insomnia).

DELETE Request to http://localhost:8090/api/customers/id/

Create a customer:

POST Request to http://localhost:8090/api/customers/

JSON Body...
{
	"first_name": "Peter",
	"last_name": "Parker",
	"address": "Queens, NY",
	"phone_number": "555-555-5555"
}


### Salespeople

GET Request to http://localhost:8090/api/salespeople/

Will display a list of salespeople showing the Name, Employee ID, and ID of each (You may use the ID number to delete the salesperson via Insomnia).

DELETE Request to http://localhost:8090/api/salespeople/id/

POST Request to http://localhost:8090/api/salespeople/

JSON Body...
{
	"first_name": "Peter",
	"last_name": "Parker",
	"employee_id": "WebHead"
}

## Inventory API
The Inventory API Consists of Manufacturers, Vehicle Models, and Automobiles, and relies on the previous ones to exist in this order.

### Manufacturers

GET Request to http://localhost:8100/api/manufacturers/

Wll display list of manufacturers names and IDs.

GET Request to http://localhost:8100/api/manufacturers/id/

Will display one specific manufacturer's information.

POST Request to http://localhost:8100/api/manufacturers/

JSON Body...
{
  "name": "Toyota"
}

PUT Request to http://localhost:8100/api/manufacturers/id/

Will update specified manufacturer with a new name.
JSON Body...
{
    "name": "Updated Name"
}

DELETE Request to http://localhost:8100/api/manufacturers/id/

Will delete specified manufacturer.

### Vehicle Models

GET Request to http://localhost:8100/api/models/

Wll display list of models names and IDs.

GET Request to http://localhost:8100/api/models/id/

Will display one specific manufacturer's information.

POST Request to http://localhost:8100/api/models/

JSON Body...
    {
    "name": "Corolla",
    "picture_url": "<url link here>",
    "manufactuerer_id": 1,
    }

This would create a Model called the Corolla and assign the manufacturer Toyota to it, and a photo.

PUT Request to http://localhost:8100/api/model/id/

Will update specified model with a new name.
JSON Body...
    {
    "name": "Supra",
    "picture_url": "<url link here>",
    "manufactuerer_id": 1,
    }

DELETE Request to http://localhost:8100/api/models/id/

Will delete specified model.

### Automobiles

Automobiles represent cars that are both currently in the inventory, as well as sold.

GET Request to http://localhost:8100/api/automobiles/

Wll display list of Automobiles' color, year, vin, model, manufacturer, availability, and IDs.

GET Request to http://localhost:8100/api/automobiles/id/

Will display one specific automobile's information.

POST Request to http://localhost:8100/api/automobiles/

JSON Body...
    {
    "color": "White/Black",
    "year": 1986,
    "vin": "1C3CC5FB2AN120005",
    "model_id": 1
    }

This would create an Automobile called the Corolla and assign the model "Corolla" to it, which would also assign the "Toyota" manufacturer.
