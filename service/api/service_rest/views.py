from django.shortcuts import render
from .models import Technician, Appointment, AutomobileVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "id",
        "import_href",
    ]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "sold_here",
    ]

    encoders = {
        "technician": TechnicianListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder = TechnicianListEncoder,
            safe=False)
    elif request.method == "POST":
        content = json.loads(request.body)
        new_technician = Technician.objects.filter(
            employee_id=content["employee_id"])
        if new_technician.exists():
            raise Exception("ID already registered")
        technician = Technician.objects.create(**content)
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianListEncoder,
            safe=False)


@require_http_methods(["GET", "DELETE"])
def api_technician_details(request, id):
    if request.method == "GET":
        technician = Technician.objects.get(id=id)
        return JsonResponse(technician,
            encoder=TechnicianListEncoder,
            safe=False)
    elif request.method == "DELETE":
        technician = Technician.objects.get(id=id)
        technician.delete()
        return JsonResponse(technician,
            encoder=TechnicianListEncoder,
            safe=False)


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder = AppointmentListEncoder,
            safe=False,)
    elif request.method == "POST":
        content = json.loads(request.body)
        technician = Technician.objects.get(
            employee_id=content["technician"])
        content["technician"] = technician
        vins = AutomobileVO.objects.all().values_list("vin", flat=True)
        content["sold_here"] = True if content["vin"] in vins else False
        content["status"] = "Created"
        appointment = Appointment.objects.create(**content)
        return JsonResponse({"appointment": appointment},
            encoder=AppointmentListEncoder, safe=False,)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_appointment_details(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(appointment,
                            encoder=AppointmentListEncoder,
                            safe=False)
    elif request.method == "PUT":
        content = json.loads(request.body)
        appointment = Appointment.objects.get(id=id)
        for key, value in content.items():
            if key == "technician":
                technician = Technician.objects.get(employee_id=value)
                appointment.technician = technician
            else:
                setattr(appointment, key, value)
        appointment.save()
        return JsonResponse({"appointment": appointment},
            encoder=AppointmentListEncoder, safe=False)
    elif request.method == "DELETE":
        appointment = Appointment.objects.get(id=id)
        appointment.delete()
        return JsonResponse({"appointment": appointment},
            encoder=AppointmentListEncoder, safe=False)


@require_http_methods(["PUT"])
def api_appointment_cancel(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.status = "Canceled"
    appointment.save()
    return JsonResponse({"appointment": appointment},
        encoder=AppointmentListEncoder, safe=False)


@require_http_methods(["PUT"])
def api_appointment_finish(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.status = "Completed"
    appointment.save()
    return JsonResponse({"appointment": appointment},
        encoder=AppointmentListEncoder, safe=False)
